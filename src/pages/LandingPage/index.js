import React from "react";
import styles from "./landingPage.module.scss";
import { FULL_LOGO } from "../../config/index";

function LandingPage() {
  return (
    <div className={styles.landingPge}>
      <img src={FULL_LOGO} alt="" />
    </div>
  );
}

export default LandingPage;
