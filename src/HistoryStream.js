import { BrowserRouter } from "react-router-dom";
import Routes from "./Routes";

function HistoryStream() {
  return (
    <BrowserRouter>
      <Routes />
    </BrowserRouter>
  );
}

export default HistoryStream;
