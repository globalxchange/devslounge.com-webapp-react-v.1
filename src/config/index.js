import fullLogo from "../static/images/logos/fullLogo.svg";
import logoIcon from "../static/images/logos/logoIcon.svg";

export const FULL_LOGO = fullLogo;
export const LOGO_ICON = logoIcon;
